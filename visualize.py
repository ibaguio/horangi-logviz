#!/usr/bin/env python
import csv
import sys

__doc__ = '''
Helper script to quickly visualize and summarize count for the columns
on logons.csv.

Usage:
  ./visualize.py <csv of indices> [filter_column_index:string_to_filter]

Example:
  Filter Source > Destination, showing only ip-addresses (filtering dots)
  ./visualize.py 4,3 4:.'''


def count_column(col=[0], filter_=None):
    domain = {}
    with open('logons.csv', 'r') as f:
        csv_reader = csv.reader(f)
        header_row = csv_reader.next()

        col_name = ','.join([header_row[int(i)] for i in col])

        for row in csv_reader:
            if filter_:
                if not filter_[1] in row[int(filter_[0])].lower():
                    continue

            # column count
            key = ','.join([row[int(i)] for i in col])
            try:
                domain[key] += 1
            except:
                domain[key] = 1




    print col_name
    for d in domain:
        print "*{}*: {}".format(d, domain[d])






def main():
    cols = sys.argv[1].split(',')
    try:
        filter_ = sys.argv[2].split(':')
    except:
        filter_ = None
    count_column(cols, filter_)


if __name__ == '__main__':
    if not len(sys.argv) > 1:
        print __doc__
    else:
        main()
