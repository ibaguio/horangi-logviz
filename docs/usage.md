## Horangi Logviz Operators Manual

Throughout this manual, let us assume that the logviz application is hosted at **logviz.horangi.com**.

### Authentication

A Basic Authentication scheme is currently implemented by the application. The default credentials are **admin:admin**


### Sources Visualization

The main component of the application is the visualization of the sources to the various destinations, and with the respecting user. The page can be found in the **sources** menu from the sidebar, or by going to http://logviz.horangi.com/sources.

Once in the sources page, two related BiPartite graphs will be displayed.

![Sources Display](images/sources.png "Sources Display")

The top area of the page shows the control buttons and filters that modify the data displayed on the graphs.

#### Filter number of entries shown

To modify the **number of entries** (*sources*) to be shown, click on the dropdown box on the upper left. The maximum option to select and entries that will be displayed is 20. This allows the operator to see more entries, as well as not get overwhelmed by a lot of items on the graph.

#### Pagination
To see more entries, the operator may select the next page (if there are) using the **page buttons** to the right of the entries dropdown.

#### Filtering the sources

Further to the right of the top bar are the **Filter dropdown**, and the **Date Range selector**. The Filter dropdown allows the operator to filter the type of log to be shown. A brief description of the filters are:

- **None** - no filter will be applied.
- **IP Network** - filters logs that are from the Network and from valid IP addresses. These requests are mostly from the public internet.
- **Other Network** - filters logs that sources are from the Network, but whose source isn't an IP address. These requests are mostly localhost connections or from a specific port.
- **Interactive** - filters logs that have Type of **Interactive**.
- **RDP** - filters logs that have Type of **RDP**

The **Date Range selector** allows the operator to narrow down the query to specific dates.

Click the **GO** button beside the Date Range selector to update the graph with the filter and date range selected.


### Heatmap Visualization

The Heatmap shows operators visibility to the timings of the logs on an hourly basis. Operators will be able to distinguish which times of the day, and which days that there are high number of logs.

![Heatmap](images/heatmap.png "Heatmap")

The heatmap also uses the similar filtering and date range selector from the Source Visualization.