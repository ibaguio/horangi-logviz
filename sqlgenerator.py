#!/usr/bin/env python
import csv
import sys

__doc__ = '''
Generates an SQL dump file that can be loaded to the MySQL database from the
give logfile.

Usage:

./sqlgenerator.py <log_csv_file_path>
'''


def generate_sqldump():
    filename = sys.argv[1]
    outfile = '.'.join(filename.split('.')[:-1]) + '.sql'

    with open(filename, 'r') as f, open(outfile, 'w') as fsql:
        csv_reader = csv.reader(f)
        header_row = csv_reader.next()      # skip the header row

        for row in csv_reader:
            if row[6][-2:] not in ['AM', 'PM']:
                dateformat = 'STR_TO_DATE("%s","%%d/%%m/%%Y %%H:%%i:%%s")'
            else:
                dateformat = 'STR_TO_DATE("%s","%%c/%%e/%%Y %%l:%%i:%%s %%p")'

            sql_row = (
                'INSERT INTO raw_log (domain,auth_protocol,logon_type,'
                'destination,source,user,timestamp,logfile,type,event_id) '
                'VALUES ("%s","%s","%s","%s","%s","%s",' + dateformat +
                ',"%s","%s","%s");'
            ) % tuple(row)

            fsql.write(sql_row + '\n')

    print 'SQL dump file:', outfile
    print 'Run command: mysql -u root -proot <', outfile


if __name__ == '__main__':
    if not len(sys.argv) > 1:
        print __doc__
    else:
        print 'Generating sqldump for', sys.argv[1],
        print (
            'Warning: This tool assumes that the given CSV file is trusted '
            'and clean.\nThis has NO built in SQL injection protection')
        generate_sqldump()
