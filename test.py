import json
import base64
import unittest
from sqlalchemy.sql import text
from sqlalchemy.orm import sessionmaker

from horangi_logviz import app, db, models, settings


class AppTest(unittest.TestCase):
    def setUp(self):
        # setup test db and fake database entries

        DB_URI = 'mysql+mysqldb://%s:%s@localhost/%s' % (
            settings.DB_USERNAME, settings.DB_PASSWORD,
            'test_' + settings.DB_NAME)

        self.app = app
        self.app.config['TESTING'] = True
        self.app.config['WTF_CSRF_ENABLED'] = False
        self.app.config['SQLALCHEMY_DATABASE_URI'] = DB_URI

        self.client = self.app.test_client()

        self.headers = {
            'Authorization': 'Basic ' + base64.b64encode('admin:admin')
        }

        dummy_data = [
            {'domain': 'horangi', 'auth_protocol': 'Kerberos', 'logon_type': 3, 'destination': 'dest_1', 'source': '1.1.1.1', 'user':'user_1', 'timestamp': '2016/01/01 00:00:02', 'logfile': '', 'type': 'Network', 'event_id': '540'},
            {'domain': 'horangi', 'auth_protocol': 'Kerberos', 'logon_type': 3, 'destination': 'dest_2', 'source': '2.2.2.2', 'user':'user_3', 'timestamp': '2016/01/01 00:00:10', 'logfile': '', 'type': 'Network', 'event_id': '540'},
            {'domain': 'horangi', 'auth_protocol': 'Kerberos', 'logon_type': 3, 'destination': 'dest_2', 'source': '2.2.2.2', 'user':'user_3', 'timestamp': '2016/01/01 00:00:10', 'logfile': '', 'type': 'Network', 'event_id': '540'},
            {'domain': 'horangi', 'auth_protocol': 'Kerberos', 'logon_type': 3, 'destination': 'dest_2', 'source': '3.3.3.3', 'user':'user_4', 'timestamp': '2016/01/01 00:00:29', 'logfile': '', 'type': 'Network', 'event_id': '540'},
            {'domain': 'horangi', 'auth_protocol': 'Kerberos', 'logon_type': 3, 'destination': 'dest_2', 'source': '3.3.3.3', 'user':'user_4', 'timestamp': '2016/01/01 00:00:29', 'logfile': '', 'type': 'Network', 'event_id': '540'},
            {'domain': 'horangi', 'auth_protocol': 'Kerberos', 'logon_type': 3, 'destination': 'dest_1', 'source': '3.3.3.3', 'user':'user_4', 'timestamp': '2016/01/01 00:01:28', 'logfile': '', 'type': 'Network', 'event_id': '540'},
            {'domain': 'horangi', 'auth_protocol': 'Kerberos', 'logon_type': 3, 'destination': 'dest_1', 'source': '1.1.1.1', 'user':'user_1', 'timestamp': '2016/01/01 00:02:03', 'logfile': '', 'type': 'Network', 'event_id': '540'},
            {'domain': 'horangi', 'auth_protocol': 'Kerberos', 'logon_type': 3, 'destination': 'dest_2', 'source': '2.2.2.2', 'user':'user_3', 'timestamp': '2016/01/01 00:02:11', 'logfile': '', 'type': 'Network', 'event_id': '540'},
            {'domain': 'horangi', 'auth_protocol': 'Kerberos', 'logon_type': 3, 'destination': 'dest_2', 'source': '2.2.2.2', 'user':'user_3', 'timestamp': '2016/01/01 00:02:11', 'logfile': '', 'type': 'Network', 'event_id': '540'},
            {'domain': 'horangi', 'auth_protocol': 'Kerberos', 'logon_type': 3, 'destination': 'dest_2', 'source': '3.3.3.3', 'user':'user_4', 'timestamp': '2016/01/01 00:02:30', 'logfile': '', 'type': 'Network', 'event_id': '540'},
            {'domain': 'horangi', 'auth_protocol': 'Kerberos', 'logon_type': 3, 'destination': 'dest_1', 'source': '1.1.1.1', 'user':'user_1', 'timestamp': '2016/01/03 00:00:02', 'logfile': '', 'type': 'Network', 'event_id': '540'},
            {'domain': 'horangi', 'auth_protocol': 'Kerberos', 'logon_type': 3, 'destination': 'dest_2', 'source': '2.2.2.2', 'user':'user_3', 'timestamp': '2016/01/03 00:00:10', 'logfile': '', 'type': 'Network', 'event_id': '540'},
            {'domain': 'horangi', 'auth_protocol': 'Kerberos', 'logon_type': 3, 'destination': 'dest_2', 'source': '2.2.2.2', 'user':'user_3', 'timestamp': '2016/01/03 00:00:10', 'logfile': '', 'type': 'Network', 'event_id': '540'},
            {'domain': 'horangi', 'auth_protocol': 'Kerberos', 'logon_type': 3, 'destination': 'dest_2', 'source': '3.3.3.3', 'user':'user_4', 'timestamp': '2016/01/03 00:00:29', 'logfile': '', 'type': 'Network', 'event_id': '540'},
            {'domain': 'horangi', 'auth_protocol': 'Kerberos', 'logon_type': 3, 'destination': 'dest_2', 'source': 'software.exe', 'user':'user_4', 'timestamp': '2016/01/03 00:00:29', 'logfile': '', 'type': 'Network', 'event_id': '540'},
        ]
        # create the database
        # with self.app.app_context():
        db.create_all()

        for data in dummy_data:
            db.session.add(models.RawLog(**data))

        db.session.commit()

    def tearDown(self):
            with self.app.app_context():
                db.drop_all()

    def test_basic_authentication(self):
        urls_get = ['/sources', '/sources/heatmap']

        urls_post = [
            '/sources/information', '/sources/user', '/sources/destination']

        for url in urls_get:
            response = self.client.get(url)
            assert response.status_code == 401

    def test_heatmap(self):
        response = self.client.post(
            '/heatmap', headers=self.headers,
            data=json.dumps({
                'start': '01-01-2016',
                'end': '01-02-2016',
                'filter_result': 'none'
            }))

        d = response.data.split('\n')
        assert d[0] == 'date,hour,value'
        assert d[1].split(',') == ['2016-01-01', '0', '10']

        response = self.client.post(
            '/heatmap', headers=self.headers,
            data=json.dumps({
                'start': '01-03-2016',
                'end': '01-04-2016',
                'filter_result': 'none'
            }))

        d = response.data.split('\n')
        assert d[0] == 'date,hour,value'
        assert d[1].split(',') == ['2016-01-03', '0', '5']

    def test_sources_user(self):
        response = self.client.post(
            '/sources/user', headers=self.headers,
            data=json.dumps({
                'start': '01-01-2016',
                'end': '01-02-2016',
                'filter_result': 'none'
            }))

        d = response.data.split('\n')
        assert d[0] == 'source,user,count'
        assert d[1] == '1.1.1.1,user_1,3'
        assert d[2] == '2.2.2.2,user_3,6'
        assert d[3] == '3.3.3.3,user_4,5'

        response = self.client.post(
            '/sources/user', headers=self.headers,
            data=json.dumps({
                'start': '01-03-2016',
                'end': '01-04-2016',
                'filter_result': 'other_network'
            }))

        # test non ip sources
        d = response.data.split('\n')
        assert d[0] == 'source,user,count'
        assert d[1] == 'software.exe,user_4,1'

    def test_sources_destination(self):
        response = self.client.post(
            '/sources/destination', headers=self.headers,
            data=json.dumps({
                'start': '01-01-2016',
                'end': '01-02-2016',
                'filter_result': 'none'
            }))

        d = response.data.split('\n')
        assert d[0] == 'source,destination,count'
        assert d[1] == '1.1.1.1,dest_1,3'
        assert d[2] == '2.2.2.2,dest_2,6'
        assert d[3] == '3.3.3.3,dest_1,1'
        assert d[4] == '3.3.3.3,dest_2,4'

if __name__ == '__main__':
    unittest.main()
