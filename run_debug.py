#!/usr/bin/env python

from horangi_logviz import app
app.run(host='0.0.0.0', port=3000, debug=True)
