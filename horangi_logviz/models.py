# import logging

from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from horangi_logviz import app, db

# logger = logging.getLogger('horangi_logviz.models')


class RawLog(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    domain = db.Column(db.String(32), nullable=True)
    auth_protocol = db.Column(db.String(32), nullable=True)
    logon_type = db.Column(db.SmallInteger)
    destination = db.Column(db.String(32))
    source = db.Column(db.String(64), nullable=True)
    user = db.Column(db.String(32), nullable=True)
    timestamp = db.Column(db.DateTime)
    logfile = db.Column(db.String(128), nullable=True)
    type = db.Column(db.String(32), nullable=True)
    event_id = db.Column(db.SmallInteger, nullable=True)

    def __init__(
            self, domain, auth_protocol, logon_type, destination,
            source, user, timestamp, logfile, type, event_id):

        self.domain = domain
        self.auth_protocol = auth_protocol
        self.logon_type = logon_type
        self.destination = destination
        self.source = source
        self.user = user
        self.timestamp = timestamp
        self.logfile = logfile
        self.type = type
        self.event_id = event_id

    @classmethod
    def insert(cls, *args):
        '''Helper method to insert a new log to the database.'''
        db.session.add(RawLog(*args))

    @classmethod
    def insery_bulk(cls, rows):
        for row in rows:
            RawLog.insert(row)

        db.session.commit()


if __name__ == '__main__':
    # flask_migration setup
    migrate = Migrate(app, db)
    manager = Manager(app)
    manager.add_command('db', MigrateCommand)
    manager.run()
