import json
import logging
import logging.config
from sqlalchemy.sql import text
from flask import Flask, Response, request, render_template, jsonify, g
from flask_sqlalchemy import SQLAlchemy

from decorators import requires_auth
from utils import get_filter_query
from horangi_logviz import app, db

# logging.config.fileConfig('logging.cfg')
logger = logging.getLogger('app')


@app.route('/sources')
@requires_auth
def sources():
    context = {
        'csv_url_dest': '/sources/destination/top',
        'csv_url_user': '/sources/user/top'
    }
    return render_template('sources.html', **context)


@app.route('/overview')
@requires_auth
def overview():
    return render_template('overview.html')


@app.route('/overview/info')
@requires_auth
def overview_info():
    conn = db.engine.connect()
    domain_count = []
    auth_protocol_count = []

    response = conn.execute(text("select count(*) from raw_log"))
    log_size_rows = "{:,}".format(response.fetchone()[0])
    print log_size_rows
    for row in conn.execute(text(
            "select domain, count(*) as `count` from raw_log "
            "group by domain order by count desc;")).fetchall():
        print row
        domain_count.append(row)

    for row in conn.execute(text(
            "select auth_protocol, count(*) as `count` from raw_log "
            "group by auth_protocol order by count desc;")).fetchall():
        print row
        auth_protocol_count.append(row)

    context = {
        'log_size_rows': log_size_rows,
        'donut_data': [domain_count, auth_protocol_count]}

    return Response(json.dumps(context), mimetype='text/json')


@app.route('/sources/heatmap')
@requires_auth
def heatmap():
    return render_template('timeseries.html')


@app.route('/heatmap', methods=['POST'])
@requires_auth
def api_heatmap():
    '''API handler that returns the heatmap data of the given filters.'''

    post_data = json.loads(request.data)
    start = post_data.get('start')
    end = post_data.get('end')
    filter_result = post_data.get('filter_result', None)

    query = get_filter_query(filter_result, """
        select DATE_FORMAT(timestamp, "%Y-%m-%d"), hour(timestamp), count(*)
        from raw_log
        where date(timestamp) between STR_TO_DATE(:start, "%m-%d-%Y")
            and STR_TO_DATE(:end, "%m-%d-%Y") {}
        group by hour(timestamp), floor(day(timestamp))
        order by timestamp""")

    logger.debug(query)

    conn = db.engine.connect()
    output = 'date,hour,value\n'

    for row in conn.execute(text(query), start=start, end=end).fetchall():
        output += '%s,%s,%s\n' % (row[0], row[1], row[2])

    return Response(output, mimetype="text/csv")


@app.route('/sources/information', methods=['POST'])
def sources_information():
    '''Returns the information about a query. Used to give a quick summary
    of a data given a filter.'''

    post_data = json.loads(request.data)
    start = post_data.get('start')
    end = post_data.get('end')
    filter_result = post_data.get('filter_result', None)

    logger.debug(post_data)
    query = get_filter_query(filter_result, """
        select count(*) as `total_logs`,
            count(distinct source) as `unique_sources` from raw_log
        where date(timestamp) between STR_TO_DATE(:start, "%m-%d-%Y")
        and STR_TO_DATE(:end, "%m-%d-%Y") {}""")

    logger.debug(query)
    conn = db.engine.connect()
    response = conn.execute(text(query), start=start, end=end).fetchone()

    # return as JSON
    return jsonify({
        'log_count': response[0], 'source_count': response[1]})


@app.route('/sources/user', methods=['POST'])
def top_sources_user():
    post_data = json.loads(request.data)
    limit = int(post_data.get('top', 10))
    page = int(post_data.get('page', 1))
    start = post_data.get('start')
    end = post_data.get('end')
    filter_result = post_data.get('filter_result', None)

    offset = int(limit) * (int(page) - 1)

    logger.debug(post_data)

    query = get_filter_query(filter_result, """
        select t1.source, user, count(*) as `count` from raw_log as t1
        inner join
        (
            select source, count(*) from raw_log where
            date(timestamp) between STR_TO_DATE(:start, "%m-%d-%Y")
            and STR_TO_DATE(:end, "%m-%d-%Y") {}
            group by source
            order by count(*) desc
            limit :limit offset :offset
        ) as t2
        on t1.source = t2.source
        group by t1.source, user""")

    conn = db.engine.connect()
    output = 'source,user,count\n'

    for row in conn.execute(
            text(query), limit=limit, start=start,
            end=end, offset=offset).fetchall():
        output += '%s,%s,%s\n' % (row[0], row[1] or 'none', row[2])

    return Response(output, mimetype="text/csv")


@app.route('/sources/destination', methods=['POST'])
def api_sources_destination():
    '''API handler that returns for the sources - destination information
    depending on the given request filters.'''

    post_data = json.loads(request.data)
    limit = int(post_data.get('top', 10))
    page = int(post_data.get('page', 1))
    start = post_data.get('start')
    end = post_data.get('end')
    filter_result = post_data.get('filter_result', None)
    offset = int(limit) * (int(page) - 1)

    logger.debug(post_data)
    query = get_filter_query(filter_result, """
        select t1.source, destination, count(*) as `count` from raw_log as t1
        inner join
        (
            select source, count(*) from raw_log where
            date(timestamp) between STR_TO_DATE(:start, "%m-%d-%Y")
            and STR_TO_DATE(:end, "%m-%d-%Y") {}
            group by source
            order by count(*) desc
            limit :limit
            offset :offset
        ) as t2
        on t1.source = t2.source
        group by t1.source, destination""")

    logger.debug(query)
    conn = db.engine.connect()

    output = 'source,destination,count\n'

    for row in conn.execute(
            text(query), limit=limit, start=start,
            end=end, offset=offset).fetchall():
        output += '%s,%s,%s\n' % (row[0], row[1], row[2])

    return Response(output, mimetype="text/csv")


if __name__ == '__main__':
    app.run(host='0.0.0.0', port='3000')
