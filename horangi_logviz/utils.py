def allowed_file(filename, allowed_extensions):
    return '.' in filename and \
        filename.rsplit('.', 1)[1].lower() in allowed_extensions


def get_filter_query(filter_result, query):
    # append additional filter queries
    if filter_result == 'ip_network':
        return query.format(
            "and source REGEXP '^[0-9]{1,3}\\."
            "[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}$' ")

    elif filter_result == 'other_network':
        return query.format(
            "and source NOT REGEXP '^[0-9]{1,3}\\."
            "[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}$'"
            " and lower(type) = 'network' ")

    elif filter_result == 'interactive':
        return query.format("and lower(type) = 'interactive'")

    elif filter_result == 'rdp':
        return query.format("and lower(type) = 'rdp'")

    else:
        return query.format('')
