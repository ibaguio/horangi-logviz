import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
# setup the database credentials from ENV, or set to root:root test default
DB_USERNAME = os.environ.get('HORANGI_DB_USERNAME', 'root')
DB_PASSWORD = os.environ.get('HORANGI_DB_PASSWORD', 'root')
DB_NAME = 'horangi'

SQLALCHEMY_DATABASE_URI = 'mysql+mysqldb://%s:%s@localhost/test_%s' % (
    DB_USERNAME, DB_PASSWORD, DB_NAME)
SQLALCHEMY_TRACK_MODIFICATIONS = False

# Basic authentication configuration to protect our app from prying eyes
AUTH_USER = os.environ.get('HORANGI_AUTH_USER', 'admin')
AUTH_PASSWORD = os.environ.get('HORANGI_AUTH_PASSWORD', 'admin')

# static and media file management
UPLOAD_FOLDER = os.path.join(BASE_DIR, 'uploads/')
