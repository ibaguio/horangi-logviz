
## HORANGI LOGVIZ

A lightweight web based log visualization tool shows a visual summary of a given logfile for easier DFIR inspection and analysis.

This application runs on Flask, MySQL, and uses SQLAlchemy to manage some of the SQL.

### Installation

##### Dependencies

	apt-get install mysql-server libmysqlclient-dev
	pip install -r horangi_logviz/requirements.txt

**Note:** It is suggested that you create a new python virtual environment for this project before installing the dependencies.

##### Database configuration and setup

1. Create a new MySQL database called **horangi**.

        mysql> create database horangi;

	If you would want to use another database name, modify the variable **DB_NAME** in **settings.py** with whatever database name you prefer.

2. Run the SQLAlchemy migration commands to create the tables.

		python models.py db migrate
		python models.py db upgrade

3. The **raw_log** table should now be created. Use the **sqlgenerator.py** tool to generate the SQL dump that can be used to load the **logons.csv** log file. Note that the tool expects the CSV file to have a specific column format of:

		Domain,Auth Protocol,Logon Type,Destination,Source,User,Date Time,Log File,Type,Event ID

	Run the command:

		sqlgenerator.py <log filename>

	This should generate an SQL file of the logfile you selected (i.e logons.sql). Load this MySQL dump file to the **horangi** database we've created on step 1.

		mysql -u <user> -p <database_name> < <log_sql_file>

	Replace the parameters as necessary.

Awesome. We should now have the log file loaded to our database.

### Running the application

Once all requirements have been installed and database configured, the flask application can easily be run for **DEVELOPMENT** by executing app.py:

	python app.py

To run the application in a more production grade environment, please check out the deployment documentation.


### Notes

0. I've decided to use MySQL as the data store for this application since a lightweight database such as sqlite might not handle the datasets especially when larger log files are loaded. Additionally, the I have more experience in developing, configuring, and administrating MySQL compared to other databases.
1. D3.js is used on all of the visualization done.
2. Bootstrap 3, jQuery, and various additional plugins are used for the UI.
